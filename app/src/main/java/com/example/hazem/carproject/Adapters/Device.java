package com.example.hazem.carproject.Adapters;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hazem.carproject.MainActivity;
import com.example.hazem.carproject.R;

import java.util.ArrayList;

/**
 * Created by Hazem on 3/9/2018.
 */

public class Device extends RecyclerView.Adapter<Device.DeviceHolder> {
    private Context context;
    private ArrayList<BluetoothDevice> Devices;

    public Device(Context context, ArrayList<BluetoothDevice> devices) {
        this.context = context;
        Devices = devices;
    }

    @Override
    public DeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.device_recy,parent,false);
        return new DeviceHolder(v);
    }

    @Override
    public void onBindViewHolder(DeviceHolder holder, int position) {
        holder.Device_Name.setText(Devices.get(position).getName());
        holder.Device_Address.setText(Devices.get(position).getAddress());
        holder.Device_BoundState.setText(""+Devices.get(position).getBondState());
     //   holder.Device_Type.setText(""+Devices.get(position).getType());
   //     holder.Device_Uuids.setText(Devices.get(position).getUuids().toString());
    }

    @Override
    public int getItemCount() {
        return Devices != null ? Devices.size():0;
    }

    public class DeviceHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView Device_Name;
        TextView Device_Address;
        TextView Device_BoundState;
    //    TextView Device_Type;
    //    TextView Device_Uuids;
        LinearLayout Device_Item;

        public DeviceHolder(View itemView) {
            super(itemView);
            Device_Name = (TextView)itemView.findViewById(R.id.Device_Name);
            Device_Address = (TextView)itemView.findViewById(R.id.Device_Address);
            Device_BoundState = (TextView)itemView.findViewById(R.id.Device_BoundState);
            Device_Item = (LinearLayout) itemView.findViewById(R.id.Device_Item);
            Device_Item.setOnClickListener(this);
        //    Device_Type = (TextView)itemView.findViewById(R.id.Device_Type);
       //     Device_Uuids = (TextView)itemView.findViewById(R.id.Device_Uuids);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.Device_Item:
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("Device_Name",Device_Name.getText().toString());
                    returnIntent.putExtra("Device_Address",Device_Address.getText().toString());
                    ((Activity)context).setResult(MainActivity.RESULT_OK,returnIntent);
                    ((Activity)context).finish();
                    break;
            }
        }
    }
}
