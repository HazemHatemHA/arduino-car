package com.example.hazem.carproject;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.hazem.carproject.Adapters.Device;

import java.util.ArrayList;
import java.util.Set;

public class ChooseDevice extends AppCompatActivity {

    private RecyclerView Devices;
    private Device adapter;
    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private ArrayList<BluetoothDevice> myDevices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDevices = new ArrayList<BluetoothDevice>();
        setContentView(R.layout.activity_choose_device);
        if (mBluetoothAdapter != null){
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                myDevices.add(device);
            }
        }
        Devices = (RecyclerView) findViewById(R.id.Devices);
        Devices.setHasFixedSize(true);
        LinearLayoutManager LL =  new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        Devices.setLayoutManager(LL);
        adapter = new Device(this, myDevices);
        Devices.setAdapter(adapter);
    }

    }
}
