package com.example.hazem.carproject;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.hazem.carproject.Bluetooth.Bluetooth;
import com.example.hazem.carproject.SensorManagment.SensorAccelerometer;
import com.example.hazem.carproject.SensorManagment.SensorManagerOwn;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;
import com.yarolegovich.lovelydialog.LovelyProgressDialog;

public class MainActivity extends AppCompatActivity implements SensorAccelerometer,View.OnClickListener ,View.OnTouchListener{

    private static final int DEVICE_CHOOSEN_DEVICE = 200;

    public final String TAG = "Main";
    private SeekBar elevation;
    private TextView debug;
    private TextView status;
    private Bluetooth bt;
    private TextView z,x,y,r_z,r_x,r_y;
    private Button Up,Down,Left,Rigth,Search_Device,Start,btn_status;
    private ToggleButton Auto;
    private  LovelyProgressDialog waitingDialog;
    private String DeviceConnectedName;
    private Context context;
    private boolean IS_ORIENTATION_LANDSCAPE = true;
    private boolean isConnected = false;
    private int Speed = 1;
    private boolean isCheckAuto = false;
    private int integer_x=0,integer_y=0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main);
        context = this;
        x = (TextView)findViewById(R.id.X) ;
        y = (TextView)findViewById(R.id.Y) ;
        z = (TextView)findViewById(R.id.Z) ;
        r_x = (TextView)findViewById(R.id.r_x) ;
        r_y = (TextView)findViewById(R.id.r_y) ;
        r_z = (TextView)findViewById(R.id.r_z) ;
        Up = (Button) findViewById(R.id.Up) ;
        Down=(Button) findViewById(R.id.Down) ;
        Left=(Button) findViewById(R.id.Left) ;
        Rigth=(Button) findViewById(R.id.Rigth) ;
        Search_Device = (Button)findViewById(R.id.Search_Device);
        Start = (Button)findViewById(R.id.start);
        Auto = (ToggleButton)findViewById(R.id.Auto);
        btn_status =(Button)findViewById(R.id.status);
        Auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isCheckAuto = isChecked;
               if(isChecked){
                   SensorManagerOwn.getInstance(MainActivity.this).StartListener();
               }else{
                   r_x.setText(""+127);
                   r_y.setText(""+127);
                //   SensorManagerOwn.getInstance(MainActivity.this).StopListener();

               }
            }
        });
        this.r_x.setText(""+127);
        this.r_y.setText(""+127);
        Up.setOnTouchListener(this);
        Down.setOnTouchListener(this);
        Left.setOnTouchListener(this);
        Rigth.setOnTouchListener(this);
        Search_Device.setOnTouchListener(this);
        Start.setOnTouchListener(this);


        debug = (TextView) findViewById(R.id.textDebug);
        status = (TextView) findViewById(R.id.textStatus);

        findViewById(R.id.restart).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DeviceConnectedName = "";
                btn_status.setText("DISCONNECTED");
                status.setText("You do not to choose any device !!");
                connectService();
            }
        });



        elevation = (SeekBar) findViewById(R.id.seekBar);
        elevation.setMax(127);
        elevation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("Seekbar","onStopTrackingTouch ");
                int progress = seekBar.getProgress();
                Speed = progress;
                String p = String.valueOf(progress);
                debug.setText(p);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("Seekbar","onStartTrackingTouch ");
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Log.d("Seekbar", "onProgressChanged " + progress);
            }
        });


        bt = new Bluetooth(this, mHandler);
        connectService();
        SensorManagerOwn.getInstance(this).setListenerSensorAccelerometer(this);
        waitingDialog= new LovelyProgressDialog(this);
        btn_status.setText("DISCONNECT");
        status.setText("You do not to choose any device !!");


    }

    public void connectService(){
        try {

            BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (bluetoothAdapter != null){
                if (bluetoothAdapter.isEnabled()) {
                    bt.start();
                    Toast.makeText(this, "Bluetooth service started", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "Btservice started - listening");
                } else {
                    Log.w(TAG, "Btservice started - bluetooth is not enabled");
                    Toast.makeText(this, "bluetooth is not enabled", Toast.LENGTH_SHORT).show();
                    status.setText("Bluetooth Not enabled");
                }
        }else Toast.makeText(this, "Not Support Bluetooth ", Toast.LENGTH_SHORT).show();
        } catch(Exception e){
            Log.e(TAG, "Unable to start bt ",e);
            Toast.makeText(this, "Unable to start Bluetooth", Toast.LENGTH_SHORT).show();
            status.setText("Unable to connect " +e);
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("IS_ORIENTATION_LANDSCAPE",IS_ORIENTATION_LANDSCAPE);
        outState.putBoolean("isConnected", isConnected);
        outState.putBoolean("isCheckAuto", isCheckAuto);
        outState.putInt("Speed", Speed);
        outState.putInt("integer_x", integer_x);
        outState.putInt("integer_y", integer_y);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        IS_ORIENTATION_LANDSCAPE = savedInstanceState.getBoolean("IS_ORIENTATION_LANDSCAPE");
        isConnected = savedInstanceState.getBoolean("isConnected");
        isCheckAuto = savedInstanceState.getBoolean("isCheckAuto");
        Speed =savedInstanceState.getInt("Speed");
        integer_x =savedInstanceState.getInt("integer_x");
        integer_y =savedInstanceState.getInt("integer_y");

    }

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Bluetooth.MESSAGE_STATE_CHANGE:
                    Toast.makeText(MainActivity.this, "MESSAGE_STATE_CHANGE", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, ": " + msg.arg1);
                    break;
                case Bluetooth.MESSAGE_WRITE:
                    Toast.makeText(MainActivity.this, "MESSAGE_WRITE", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "MESSAGE_WRITE ");
                    break;
                case Bluetooth.MESSAGE_READ:
                    Toast.makeText(MainActivity.this, "MESSAGE_READ", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "MESSAGE_READ ");
                    break;
                case Bluetooth.MESSAGE_DEVICE_NAME:
                    Bundle data = msg.getData();
                    waitingDialog.dismiss();
                    if (data != null) {
                        String deviceName = (String) data.get("Connected");
                        new LovelyInfoDialog(context)
                                .setTopColorRes(R.color.colorPrimary)
                                .setTitle("Success")
                                .setMessage("Connected  successfully :" + deviceName)
                                .show();
                        isConnected = true;
                        btn_status.setText("CONNECTED");
                        DeviceConnectedName = deviceName;
                    }
                    break;
                case Bluetooth.MESSAGE_TOAST:
                    Log.d(TAG, "MESSAGE_TOAST " + msg);
                    Bundle recivedData = msg.getData();
                    if (recivedData != null) {
                        try {
                            String massage = (String) recivedData.get("Toast");
                            waitingDialog.dismiss();
                            new LovelyInfoDialog(context)
                                    .setTopColorRes(R.color.colorError)
                                    .setTitle("Error ")
                                    .setMessage(massage)
                                    .show();

                        } catch (Exception e) {
                        }


                        break;
                    }
            }
        }
    };



    @Override
    protected void onResume() {
        super.onResume();
        SensorManagerOwn.getInstance(this).StartListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onSensorChangedAccelerometer(String x , String y , String Z) {
        if(isCheckAuto) {
            if(IS_ORIENTATION_LANDSCAPE) {
                this.r_x.setText(x);
                this.r_y.setText(""+(-1 * Integer.parseInt(y)));
                this.r_z.setText(Z);
            }else {
                this.r_x.setText(x);
                this.r_y.setText(y);
                this.r_z.setText(Z);
            }
        }
        SendMessage();


    }
    private void SendMessage(){
        if(isConnected) {
            integer_x= r_x.getText().toString().isEmpty()?0: Integer.parseInt(r_x.getText().toString());
            integer_y= r_y.getText().toString().isEmpty()?0: Integer.parseInt(r_y.getText().toString());
            String result;
            if(!IS_ORIENTATION_LANDSCAPE) {
                 result = GetNumber(integer_x) + "" + GetNumber(integer_y);
            }else  result = GetNumber(integer_y) + "" + GetNumber(integer_x);
             bt.sendMessage("TM" +result);
          }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            IS_ORIENTATION_LANDSCAPE = true;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            switch (v.getId()){

                case R.id.Up:Up();Log.d("SSS","Up");break;
                case R.id.Down:Down();Log.d("SSS","Dwon");break;
                case R.id.Left:Left();Log.d("SSS","Left");break;
                case R.id.Rigth:Right();Log.d("SSS","Rigth");break;
                case R.id.Search_Device:Search_Device();break;
                case R.id.start:StartConnection();break;
                case R.id.restart:StopConnecton();break;

            }
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            r_x.setText(""+127);
            r_y.setText(""+127);
        }
        return true;
    }

    private void StartConnection(){
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null){
            if (bluetoothAdapter.isEnabled()) {
                if (DeviceConnectedName != null && !DeviceConnectedName.isEmpty()){
                    waitingDialog.setCancelable(false)
                            .setTitle("Connecting...")
                            .setMessage("Contected to :"+this.DeviceConnectedName)
                            .setTopColorRes(R.color.colorPrimary)
                            .show();
                    bt.connectDevice(DeviceConnectedName);
            }else{
                    new LovelyInfoDialog(context)
                            .setTopColorRes(R.color.colorPrimary)
                            .setTitle("Error")
                            .setMessage("Please choose Device to connect !! :")
                            .show();
                }
                Toast.makeText(this, "Bluetooth service started", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Btservice started - listening");
            } else {
                Log.w(TAG, "Btservice started - bluetooth is not enabled");
                Toast.makeText(this, "bluetooth is not enabled", Toast.LENGTH_SHORT).show();
                status.setText("Bluetooth Not enabled");
            }
        }else Toast.makeText(this, "Not Support Bluetooth ", Toast.LENGTH_SHORT).show();

    }


    private void StopConnecton(){
        bt.stop();
        isConnected = false;
    }


    private void UpdateResult(){
        int x = Integer.parseInt(r_x.getText().toString());
        int y = Integer.parseInt(r_y.getText().toString());
        String result= "TM"+GetNumber(x)+GetNumber(y);
        this.r_z.setText(result);
    }

    private String GetNumber(int num){
        if(num < 10) return "00"+num;
        else if(num < 100)return "0"+num;
        else return num+"";
    }

    private void Up(){
        this.r_x.setText(""+ValueInRange(127-Speed));
        UpdateResult();
    //    SendMessage();
    }
    private void Down(){
            this.r_x.setText("" + ValueInRange(127+Speed));

        UpdateResult();
     //   SendMessage();
    }
    private void Left(){
        this.r_y.setText(""+ValueInRange(127+Speed));
        UpdateResult();
    //    SendMessage();
    }
    private void Right(){
     //   Inte x = Float.parseFloat(r_x.getText().toString());
            this.r_y.setText("" + ValueInRange(127-Speed));
        UpdateResult();
     //   SendMessage();
    }

    private int ValueInRange(int number){
        if(number > 255) return 255;
        if(number < 0) return 0;
        return number;
    }

    private void Search_Device(){
        Intent goToSearch = new Intent(this,ChooseDevice.class);
        startActivityForResult(goToSearch,DEVICE_CHOOSEN_DEVICE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEVICE_CHOOSEN_DEVICE) {
            if (resultCode == RESULT_OK) {
                String Device_Name = data.getStringExtra("Device_Name");
                String Device_Address =data.getStringExtra("Device_Address");
                DeviceConnectedName = Device_Name;
                status.setText("ready to connected to device :"+DeviceConnectedName);

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bt.stop();
    }


}


