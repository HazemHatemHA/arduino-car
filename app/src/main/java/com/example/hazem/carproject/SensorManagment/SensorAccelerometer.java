package com.example.hazem.carproject.SensorManagment;

import android.hardware.SensorEvent;

/**
 * Created by Hazem on 3/9/2018.
 */

public interface SensorAccelerometer {
    void onSensorChangedAccelerometer(String x , String y , String Z);
}
