package com.example.hazem.carproject.SensorManagment;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by Hazem on 3/8/2018.
 */

public class SensorManagerOwn  implements SensorEventListener{
    private Context context;
    private static SensorManagerOwn Instance;
    private SensorManager mSensorManager;
    private Sensor ACCELEROMETER;
    private SensorAccelerometer mSensorAccelerometer;

    public static SensorManagerOwn getInstance(Context context){
        if(Instance == null)Instance = new SensorManagerOwn(context);
        return Instance;
    }

    public void setListenerSensorAccelerometer(SensorAccelerometer mSensorAccelerometer){
        this.mSensorAccelerometer = mSensorAccelerometer;
    }



    public SensorManagerOwn(Context context) {
        mSensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
        ACCELEROMETER = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public SensorManagerOwn(Context context ,SensorAccelerometer mSensorAccelerometer){
        this.context = context;
        this.mSensorAccelerometer = mSensorAccelerometer;
    }

    public void StartListener(){
        mSensorManager.registerListener(this,ACCELEROMETER,SensorManager.SENSOR_DELAY_NORMAL);
    }
    public void StopListener(){
        mSensorManager.unregisterListener(this);

    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        NumberFormat formatter = new DecimalFormat("000");
        String result= "TM"+formatter.format(getnumber(event.values[0]))+formatter.format(getnumber(event.values[1]));
        mSensorAccelerometer.onSensorChangedAccelerometer(""+getnumber(event.values[0]), ""+getnumber(event.values[1]), result);
    }
    public int getnumber(float f){
        if (f<-5)f=-5;
        if (f>5)f=5;
        if (f<1 && f>-1)f=0;
        f+=5;
        f*=25.4;

        return (int)f;

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //  Toast.makeText(this, ""+sensor.getName().toString(), Toast.LENGTH_SHORT).show();

    }

    public SensorManager getmSensorManager() {
        return mSensorManager;
    }

    public void setmSensorManager(SensorManager mSensorManager) {
        this.mSensorManager = mSensorManager;
    }

    public Sensor getACCELEROMETER() {
        return ACCELEROMETER;
    }

    public void setACCELEROMETER(Sensor ACCELEROMETER) {
        this.ACCELEROMETER = ACCELEROMETER;
    }


}
